## 1.  Lancer un MySQL 5.7 sur votre ordinateur avec Docker
    
*Besoin d’aide ?*
Chercher une **image officielle** de MySQL
Utiliser la commande `docker run`

## 2.  Récupérez 5 images docker et supprimez les toutes

## 3.  Ajoutez des données dans votre conteneur mySQL

*Besoin d’aide ?*
Utiliser la commande `docker exec`.
La connexion à mysql utilise par défaut les **identifiants root** et **mot de passe root**

## 4.  Supprimez et relancez un conteneur. Que constatez vous ? Qu’est-il arrivé à vos données ? Que remarquez vous à propos du container ?
  

## 5.  Essayez de créer un volume avec votre conteneur, et faites y persister vos données. Supprimez votre conteneur et relancez en un.
   

*Besoin d’aide ?*
Utilisez l’option **-v**  
  

## 6.  Lancez l’image tutum/hello-world et affichez le dans votre navigateur préféré
   
*Besoin d’aide ?*
tutum/hello-world ouvre le **port 80** et permet d’afficher hello world sur le protocole http  


Vous pouvez maintenant passer à la partie 2 de la présentation