
# Cara Docker presentation

## Back part

The back part is in back directory. Execute all following command in back directory: `cd back`

Firstly, you need to add multiple environment variable
```
DATABASE_HOST
DATABASE_PORT
DATABASE_NAME
DATABASE_USERNAME
DATABASE_PASSWORD
```
To do that, you can fill the .env file and execute `export $(cat .env | xargs)`

*Remember, this project works with mysql database.*

Then, you can run the back following steps :
```
mvn clean install
mvn spring-boot:run
```

Check if your back is up : `http://localhost:8080/ping`

## Front part

The back part is in back directory. Execute all following command in back directory: `cd front`

You can start the front project just running these following
```
npm install
npm start
```

# Your job

You need to dockerized the integrality of the project.

The objectif, one command `docker-compose up -d` and we can hit the `ping` route or the main page of Front part.

To help you, you can follow these steps
1. Dockerize back part 
	- The project work with database, don't forget it ;) 
	- Open the back directory and fill the empty Dockerfile.
	- You can run it with docker command.
	- You can try if it work by the way of ping route `http://<ip_adresse>:80/ping`.
2. Dockerize the front part 
	- Open the front directory and fill the empty Dockerfile.
	- You can run it with docker command.
	- You can try if it work by the way of open your favorite web browser at `http://<ip_adresse>:<allowed_port>`.
3. Dockercompose
	- Go to the root directory and open the docker-copose.yml file
	- You need to fill the 3 services. 
	- Try if it work by running the docker-compose command  and go to  `http://<ip_adresse>:<allowed_port>`.