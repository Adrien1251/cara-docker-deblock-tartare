package com.example.cara.bddcara.controller;

import com.example.cara.bddcara.entity.Rank;
import com.example.cara.bddcara.service.RankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/ranks")
public class RankController {
    @Autowired
    private RankService rankService;

    @GetMapping
    public List<Rank> getRanks(){
        return this.rankService.getRanks();
    }

    @PostMapping("/{userName}")
    public Rank updateRanks(@PathVariable String userName){
        return this.rankService.createOrUpdateRank(userName);
    }
}
