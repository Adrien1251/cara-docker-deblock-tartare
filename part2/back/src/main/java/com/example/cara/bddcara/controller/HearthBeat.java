package com.example.cara.bddcara.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/ping")
public class HearthBeat {
    @GetMapping
    public ResponseEntity<String> pong(){
        return ResponseEntity.status(HttpStatus.OK).body("Pong");
    }
}
