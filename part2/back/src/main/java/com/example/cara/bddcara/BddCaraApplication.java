package com.example.cara.bddcara;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BddCaraApplication {

	public static void main(String[] args) {
		SpringApplication.run(BddCaraApplication.class, args);
	}

}
