package com.example.cara.bddcara.repository;

import com.example.cara.bddcara.entity.Rank;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RankRepository extends JpaRepository<Rank, Long> {
    List<Rank> findAllByOrderByPoints();
    Optional<Rank> findByUserName(String userName);
}
