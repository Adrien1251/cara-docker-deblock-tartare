package com.example.cara.bddcara.service;

import com.example.cara.bddcara.entity.Rank;
import com.example.cara.bddcara.repository.RankRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RankService {

    @Autowired
    private RankRepository rankRepository;

    public List<Rank> getRanks() {
        return this.rankRepository.findAllByOrderByPoints();
    }

    public Rank createOrUpdateRank(String userName) {
        Optional<Rank> rankDb = this.rankRepository.findByUserName(userName);
        if(rankDb.isPresent()){
            rankDb.get().setPoints(rankDb.get().getPoints() + 1001);
            return this.rankRepository.save(rankDb.get());
        }

        return this.rankRepository.save(Rank.builder()
                .userName(userName)
                .points(1000l)
                .build());
    }
}
