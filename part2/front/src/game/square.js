import React from 'react';

function Square(props) {
    return (
      <button className="square" onClick={props.onClick}>
        {props.value}
      </button>
    );
  }
  
  class Board extends React.Component {
    renderSquare(i) {
      return (
        <Square
          value={this.props.squares[i]}
          onClick={() => this.props.onClick(i)}
        />
      );
    }
  
    render() {
      return (
        <div>
          <div className="board-row">
            {this.renderSquare(0)}
            {this.renderSquare(1)}
            {this.renderSquare(2)}
          </div>
          <div className="board-row">
            {this.renderSquare(3)}
            {this.renderSquare(4)}
            {this.renderSquare(5)}
          </div>
          <div className="board-row">
            {this.renderSquare(6)}
            {this.renderSquare(7)}
            {this.renderSquare(8)}
          </div>
        </div>
      );
    }
  }
  
  export default class Game extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        player1: 'player1',
        player2: 'player2',
        history: [
          {
            squares: Array(9).fill(null)
          }
        ],
        stepNumber: 0,
        xIsNext: true,
        ranks: [],
      };
    }
  
    handleClick(i) {
      const history = this.state.history.slice(0, this.state.stepNumber + 1);
      const current = history[history.length - 1];
      const squares = current.squares.slice();
      if (calculateWinner(squares) || squares[i]) {
        return;
      }
      squares[i] = this.state.xIsNext ? "X" : "O";
      this.setState({
        alreadyUp: false,
        history: history.concat([
          {
            squares: squares
          }
        ]),
        stepNumber: history.length,
        xIsNext: !this.state.xIsNext
      });
      
      this.BACK_URL = 'http://192.168.99.100:8080/';
    }
    componentDidMount() {
      this.getAllRanks();
    }

    getAllRanks(){
      fetch('http://192.168.99.100:8080/ranks')
      .then(response => response.json())
      .then(data => this.setState({ ranks: data }));
    }

    updateRank(userName){
      fetch('http://192.168.99.100:8080/ranks/'+userName, {
        method: 'POST'
      })
      .then(response => response.json())
      .then(data => this.getAllRanks());
    }

    jumpTo(step) {
      if(step === 0) this.state.alreadyUp = true;
      this.setState({
        stepNumber: step,
        xIsNext: (step % 2) === 0
      });
    }
  
    render() {
      const history = this.state.history;
      const current = history[this.state.stepNumber];
      const winner = calculateWinner(current.squares);
  
      let ranks = this.state.ranks.map((rank) => {
        return (
          <li key={rank.id}>
            {rank.userName}: {rank.points}
          </li>
        );
      });
      if(ranks.length === 0) {
        ranks = <p>No rank found in database</p>
      }
      
      let status;
      let restart;
      if (winner || needRestartButton(current.squares)) {
        const userNameWinner = winner === 'O' ? this.state.player2 : this.state.player1
        status = winner ? "Winner: " + userNameWinner : "No winner, restart ?";
        if(!this.state.alreadyUp) {
          this.updateRank(userNameWinner);
          this.state.alreadyUp = true;
        }
        restart = <button onClick={() => this.jumpTo(0)}>Restart Game</button>;
      } else {
        status = (this.state.xIsNext ? 
            (this.state.player1) : 
            (this.state.player2)
          );
      }
  
      return (
        <div className="game">
          <div className="player-info">
            <h2>Player information</h2>
            <div>
              <label>Player 1 name: </label>
              <input type="text" value={this.state.player1} onChange={(event) => this.setState({player1:event.target.value})}/>
            </div>
            <div>
              <label>Player 2 name: </label>
              <input type="text" value={this.state.player2} onChange={(event) => this.setState({player2:event.target.value})} />
            </div>
            <div>
              {restart}
            </div>
          </div>
          <div className="game-panel">
            <div className="game-info">
              <div>{status}</div>
            </div>
            <div className="game-board">
              <Board
                squares={current.squares}
                onClick={i => this.handleClick(i)}
              />
            </div>
          </div>
          <div className="rank-panel">
            <h2>Ranks</h2>
            {ranks}
          </div>
        </div>
      );
    }
  }
  
  // ========================================
  
  function needRestartButton(squares){
    return squares.filter(squareValue => squareValue === null).length === 0;
  }

  function calculateWinner(squares) {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6]
    ];
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
        return squares[a];
      }
    }
    return null;
  }
  