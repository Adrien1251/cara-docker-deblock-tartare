# Cara Docker presentation

This presentation contains two parts. 

The first one is some questions to learn how docker works.

The second is to dockerize an entire project. (Here is Spring boot / React Js project) 

# Steps 

1. Open the part1 directory and answer the all questions 

2. Open the part2 directory and follow the README.md file steps.

# Made by 

This presentation is made by Valentin Tartare and Adrien Deblock